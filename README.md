## LMNP ?
---
The LMNP stack stands for Linux, MySQL, Nginx (read as engine-x) and PHP. This are the common tools we install in our machine when we start developing websites using PHP.

Installing these tools on different computers causes the problems with inconsistencies. 

So let’s solve this by having a common Docker configuration! Let’s use Docker to setup our PHP development environment.

Let’s start !

<code>git clone git@gitlab.com:oktech/docker-lmnp.git</code>

## For a simple php project
---

NB : clone the project remove the symfony and laravel folders.

Put the php, the nginx folders and the docker-compose file inside your project project and you are ready to Go !

## For a Symfony project
---

NB : Use only the symfony folder of the repo.

Put the docker folder and the docker-compose file inside your symfony project and you are ready to Go !

## For a Laravel project (comming soon ...)
---

NB : Use only the Laravel folder of the repo.

Put the docker folder and the docker-compose file inside your Laravel project and you are ready to Go !

## Running Docker services
---

Now let’s see Docker in action by opening your favorite command line tool and run this command in your project root:

<code>
    $ docker-compose -f docker-compose.yml up
</code>

The first time you’ll run this command it will take time be as it will download necessary base images and build the services based on our configuration. You’ll know all services are up and running if you don’t see the logs displayed moving 😉

Go to http://localhost:8080. You should see the your first Hello World using docker!

Now we can commit and push our project on GitHub. Throw away our computer. Buy a new one. Install Docker and Git. Clone your project from Gitlab/GitHub.

Run <code>docker-compose -f docker-compose.yml up</code> and still get the 
same result every time !

## What’s next?  
---

Enjoy your new development setup 😉

Cheers!